<VirtualHost *:[port]>
	ServerAdmin webmaster@localhost

	DocumentRoot [doc_root]/
	<Directory />
		Options Indexes FollowSymLinks MultiViews
		AllowOverride None
		Require all denied
	</Directory>
	<Directory [doc_root]>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride None
		Require all granted
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error-[port].log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

	CustomLog ${APACHE_LOG_DIR}/access-[port].log combined

</VirtualHost>
