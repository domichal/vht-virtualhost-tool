VirtualHostTool
===============

Configurable bash script to create/remove apache2 virtualhosts in seconds based on a template file

This script can create or remove apache2 virtualhost.

OPTIONS:
--------
   -h  show this message
   
   -a	add (default), requires -d, -p
   
   -r	remove, requires -p		
   
   -d	Document Root for new vhost
   
   -p	Port number

USAGE:
------
* adding virtualhost

```
sudo bash ./vht.sh -p 8000 -d workspace/project
```
* removing virtualhost (works with vhosts created with this tool only!)

```
sudo bash ./vht.sh -r -p 8000
```

CONFIGURATION:
--------------
see vht.ini file

TEMPLATE:
---------
see vht.tpl template file