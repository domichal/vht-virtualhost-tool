#!/bin/bash

CURRENT=`cd $( dirname "${BASH_SOURCE[0]}" ); pwd`
source $CURRENT"/vht.ini"
nvh_fn=
action="add"
port=
dir=

function inf(){
cat << EOF
usage: $0 options

This script can create or remove apache2 virtualhost.

OPTIONS:
   -h	show this message
   -a	add (default), requires -d, -p
   -r	remove, requires -p		
   -d	Document Root for new vhost
   -p	Port number

USAGE:
* adding virtualhost
$ sudo bash ./vht.sh -p 8000 -d workspace/project
* removing virtualhost (works with vhosts created with this tool only!)
$ sudo bash ./vht.sh -r -p 8000

CONFIGURATION:
see vht.ini file

TEMPLATE:
see vht.tpl template file

EOF
}

while getopts “hard:p:” OPTION
do
	case $OPTION in
		h)
			inf
			exit 1
			;;
		a)
			action="add"
			;;
		r)  
			action="remove"
			;;
		d)
			dir=$OPTARG
			;;
		p)
			port=$OPTARG
			;;
	esac
done

function check_root(){
	if [ "$(id -u)" != "0" ]; then
		err "You must be logged in as root"
		exit 0
	fi
}

function check_vars(){
	ok=1
	if ! [ -r $template ]; then
		ok=0; err "file $template does not exist"
	fi
	if ! [ -d $vhosts_dir ]; then
		ok=0; err "directory $vhosts_dir does not exist"
	fi
	if ! [ -w $ports_conf ]; then
		ok=0; err "file $ports_conf does not exist or is write protected"
	fi
	if [ "$ok" = "0" ]; then
		exit 0
	fi
}

function validate_params(){
	ok=1
	if [ "$action" = "add" ]; then
		if ! [ $dir ] || ! [ $port ]; then
			err "both port and dir parameters are required for action $action, displaying help..."
			inf
			exit 1
		fi		
		if [ -d "$dir" ]; then
			dir=`cd $dir; pwd`
		else
			err "directory does not exist: $dir"
			ok=0
		fi
	elif [ "$action" = "remove" ]; then
		if ! [ $port ]; then
			err "port parameter is required for action $action, displaying help..."
			inf
			exit 1
		fi
	fi	
	if [[ "$port" =~ ^[0-9]{3,6}$ ]]; then
		if ! [ -z "$(netstat -antp | grep "\s$port\s")" ]; then
			err "port in use: $port"
			ok=0
		fi
	else
		err "incorrect port format: $port"
		ok=0
	fi
	if [ "$ok" = "0" ]; then
		exit 1
	fi
}

function name_increment(){
	new_no=$(( $(ls $vhosts_dir | grep -o '^[0-9]\{3\}' | tail -1 | sed 's/0*//')+1 ))
	for (( c=0; c<=$(( 3-${#new_no} )); c++ )); do
		new_no="0$new_no"
	done
	nvh_fn="$new_no-VirtualHost-$port"
	return 0
}

function add_port(){
	
	if [ -z "$(cat $ports_conf | grep "^Listen\s$port$")" ]; then
		nl_listen=$(cat $ports_conf | grep -n '^Listen.*' | tail -1 | grep -o '^[0-9]*')
		echo "$(sed "$nl_listen a Listen $port" $ports_conf)" > $ports_conf
		upd=1
	else
		confirm "File $ports_conf already contains line \"Listen $port\""
		log "line \"Listen $port\" exists in $ports_conf, skipping..."		
	fi
	
	if [ $upd ]; then
		log "file $ports_conf updated with new entries for port $port"
	fi
	#echo "new line for listen: $nl_listen" 
	#echo "new line for virtual host: $nl_nvh" 
	#echo "saving port $port"
	return 0
}

function save_new_site(){
	if ! [ -z "$(ls $vhosts_dir | grep "\-VirtualHost\-$port$")" ]; then
		confirm "VirtualHost using port $port seem to be already present"
	fi
	name_increment
	echo "$(cat $template | sed 's/\[port\]/'$port'/' | sed 's/\[doc_root\]/'$(echo "$dir" | sed 's/\//\\\//g')'/')" > "$vhosts_dir$nvh_fn.conf"
	log "virtualhost file $vhosts_dir$nvh_fn.conf created"
	return 0
}

function remove_virtualhost(){
	if [ -z "$(ls $vhosts_dir | grep "\-VirtualHost\-$port$")" ]; then
		err "virtualhost *:$port file not found, leaving."
		exit 0
	fi
	echo "$(sed "/^Listen\s$port$/d" < $ports_conf)" > $ports_conf
	ls $vhosts_dir | grep "^[0-9]\{3\}\-VirtualHost\-$port$" | while read vhost; do
		rm "$vhosts_dir$vhost"
	done
	log "file(s) removed"
}

function log(){
	echo " * $1"
}

function err(){
	echo "[!] $1"
}

function confirm(){
	read -p "$1, continue? " -n 1; echo
	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		exit 1
	fi
}

check_root
check_vars
validate_params
if [ "$action" = "add" ]; then
	add_port
	save_new_site
	log "VirtualHost successfully added."
elif [ "$action" = "remove" ]; then
	remove_virtualhost
	log "VirtualHost successfully removed."
fi
if [[ $restart_apache =~ ^yes|YES|Yes$ ]]; then
	service apache2 restart
	#/etc/init.d/apache2 restart
fi
exit 1
